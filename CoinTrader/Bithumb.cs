﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using CoinTrader;

class Bithumb : DriverController
{

    public Form1 uiController = null;

    public Bithumb(Form1 uiController_ = null)
    {
        CreatDriver(TYPE_DRIVER.TYPE_Chrome);
        uiController = uiController_;
    }

    void PrintLog(string msg_)
    {
        if (uiController == null)
            return;

        uiController.PrintLog(msg_);
    }

    public bool Login()
    {
        driver.Navigate().GoToUrl("https://www.bithumb.com/trade/order/XRP");

        Thread.Sleep(1000);
        CollectCoinList();

        driver.Navigate().GoToUrl("https://www.bithumb.com/u1/US101");

        return true;
    }

    void CollectCoinList()
    {
        //*[@id="aside_coin"]/div[2]/ol
        //*[@id="_wModal_pixel"]/div/div
        try
        {
            Global.listCoin.Clear();
            IList<IWebElement> listElemCoin = FindMyElements(By.XPath("//*[@id='aside_coin']/div[2]/ol/li"));
            foreach(var item in listElemCoin)
            {
                string coinName = item.GetAttribute("data-coin");
                if (string.IsNullOrEmpty(coinName) == false)
                    Global.listCoin.Add(coinName);
            }
        }
        catch
        {

        }
    }

    public bool GoTradePage()
    {
        if (string.IsNullOrEmpty(Global.comboBox_TradeCoin))
        {
            PrintLog("거래할 코인을 선택해주세요");
            return false;
        }

        driver.Navigate().GoToUrl("https://www.bithumb.com/trade/order/" + Global.comboBox_TradeCoin);

        Thread.Sleep(500);

        ClickTradedList();
        //클릭
        

        return true;
    }

    void ClickTradedList()
    {
        try
        {
            IWebElement btn = driver.FindElementByXPath("//*[@id='contractsTab']/dt/a[2]");
            btn.Click();
            Thread.Sleep(100);
        }
        catch
        {
            return;
        }
    }

    public string GetCointTitle()
    {
        try
        {
            return driver.FindElementByXPath("//*[@id='contents']/h2/strong").Text;
        }
        catch
        {
            return "";
        }
    }

    public string GetCurrentPrice()
    {
        try
        {
            return driver.FindElementByXPath("//*[@id='contents']/article/div[1]/div/div/h3").Text;
        }
        catch
        {
            return "";
        }
    }

    public double GetTadeFee()
    {
        try
        {
            //*[@id="coinFeeBuy"]
            string fee = driver.FindElementByXPath("coinFeeBuy").Text;
            return double.Parse(fee);
        }
        catch
        {
            return 0.0f;
        }
    }

    public string GetMyAmount()
    {
        try
        {
            //*[@id="coinAvail"]
            return driver.FindElementByXPath("//*[@id='coinAvail']").Text;
        }
        catch
        {
            return "";
        }
    }

    public string GetMyMoney()
    {
        try
        {
            //*[@id="krwAvail"]
            return driver.FindElementByXPath("//*[@id='krwAvail']").Text;
        }
        catch
        {
            return "";
        }
    }

    public string GetTradingValue()
    {
        try
        {
            //*[@id="krwAvail"]
            //return driver.FindElementByXPath("//*[@id='krwUse']").Text;
            long ret = 0;
            IList<IWebElement> listTrade = FindMyElements(By.XPath("//*[@id='contConstracts']/tbody/tr"));
            foreach(var item in listTrade)
            {
                string htmlString = item.GetAttribute("innerHTML");
                if (htmlString.Contains("거래 내역이 없습니다."))
                    break;

                //*[@id="contConstracts"]/tbody/tr[1]/td[6]
                string tradeValue = item.FindElement(By.XPath(".//td[6]")).Text;
                tradeValue = tradeValue.Replace(",", "");
                long value = long.Parse(tradeValue);
                //*[@id="contConstracts"]/tbody/tr[1]/td[2]
                string tradeType = item.FindElement(By.XPath(".//td[2]")).Text;
                if (tradeType.Contains("매수"))
                    ret += value;
                else
                    ret -= value;

            }

            return ret.ToString();

        }
        catch
        {
            return "";
        }
    }

    public bool ClickBuyTab()
    {
        try
        {
            IWebElement purchaseTab = driver.FindElementByXPath("//*[@id='contents']/article/div[2]/dl/dt/a[1]");
            string htmlString = purchaseTab.GetAttribute("innerHTML");
            if (htmlString.Contains("class='on'"))
                return true;
            else
                purchaseTab.Click();
            Thread.Sleep(100);

            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool ClickSellTab()
    {
        try
        {
            IWebElement purchaseTab = driver.FindElementByXPath("//*[@id='contents']/article/div[2]/dl/dt/a[2]");
            string htmlString = purchaseTab.GetAttribute("innerHTML");
            if (htmlString.Contains("class='on'"))
                return true;
            else
                purchaseTab.Click();
            Thread.Sleep(100);

            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool InputBuyPrice(long value_)
    {
        try
        {
            IWebElement priceEdit = driver.FindElementByXPath("//*[@id='coinAmtCommaBuy']");
            if (priceEdit.Text == value_.ToString())
                return true;
            else
            {
                priceEdit.Clear();
                Thread.Sleep(10);
                priceEdit.SendKeys(value_.ToString());
            }
                
            Thread.Sleep(100);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool InputSellPrice(long value_)
    {
        try
        {
            IWebElement priceEdit = driver.FindElementByXPath("//*[@id='coinAmtCommaSell']");
            if (priceEdit.Text == value_.ToString())
                return true;
            else
            {
                priceEdit.Clear();
                Thread.Sleep(10);
                priceEdit.SendKeys(value_.ToString());
            }

            Thread.Sleep(100);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool InputBuyAmount(float value_)
    {
        try
        {
            IWebElement amounteEdit = driver.FindElementByXPath("//*[@id='coinQtyBuy']");
            amounteEdit.Clear();
            amounteEdit.SendKeys(value_.ToString());
            Thread.Sleep(100);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool InputSellAmount(float value_)
    {
        try
        {
            IWebElement amounteEdit = driver.FindElementByXPath("//*[@id='coinQtySell']");
            amounteEdit.Clear();
            amounteEdit.SendKeys(value_.ToString());
            Thread.Sleep(100);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool ClickBuy()
    {
        //*[@id="btnBuy"]
        try
        {
            IWebElement bntBuy = driver.FindElementByXPath("//*[@id='btnBuy']");
            bntBuy.Click();
            Thread.Sleep(200);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool ClickSell()
    {
        try
        {
            IWebElement bntBuy = driver.FindElementByXPath("//*[@id='btnSell']");
            bntBuy.Click();
            Thread.Sleep(200);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool CheckConfirmBox()
    {
        try
        {
            IList<IWebElement> listPopup = FindMyElements(By.XPath("//*[@id='_wModal_pixel']/div"));

            foreach(var item in listPopup)
            {
                string attStyle = item.GetAttribute("style");
                if (attStyle.Contains("display: none;"))
                    continue;

                string _msg = item.FindElement(By.ClassName("_wModal_msg")).Text;
                PrintLog(_msg);

                IWebElement btnConfirm = item.FindElement(By.Id("wModalBtnApprove"));
                btnConfirm.Click();
                Thread.Sleep(200);
                return true;
            }

            return false;

        }
        catch
        {
            return false;
        }
    }

    public bool ClickMaxAmount()
    {
        try
        {
            //*[@id="otherTradeDivBuy"]/tbody/tr[4]/td/div[2]/div[2]/a
            IWebElement btnMax = driver.FindElementByXPath("//*[@id='otherTradeDivBuy']/tbody/tr[4]/td/div[2]/div[2]/a");
            btnMax.Click();
            Thread.Sleep(100);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool ClickApproveOk()
    {
        try
        {
            IList<IWebElement> listPopup = FindMyElements(By.XPath("//*[@id='_wModal_pixel']/div"));

            foreach (var item in listPopup)
            {
                string attStyle = item.GetAttribute("style");
                if (attStyle.Contains("display: none;"))
                    continue;

                string _msg = item.FindElement(By.ClassName("_wModal_msg")).Text;
                PrintLog(_msg);             

                IWebElement btnConfirm = item.FindElement(By.Id("wModalBtnYes"));
                btnConfirm.Click();
                Thread.Sleep(200);

                if (_msg.Contains("잠시 후 다시 시도해 주세요"))
                    return false;

                return true;
            }

            return false;
        }
        catch
        {
            return false;
        }
    }

    public string ClearAlertPopup()
    {
        try
        {
            if (driver.PageSource.Contains("_wModal_pixel") == false)
                return "";

            IList<IWebElement> listPopup = FindMyElements(By.XPath("//*[@id='_wModal_pixel']/div"));

            foreach (var item in listPopup)
            {
                string attStyle = item.GetAttribute("style");
                if (attStyle.Contains("display: none;"))
                    continue;

                string _msg = item.FindElement(By.ClassName("_wModal_msg")).Text;
                PrintLog(_msg);

                IWebElement btnConfirm = item.FindElement(By.Id("wModalBtnYes"));
                btnConfirm.Click();
                Thread.Sleep(100);
                return _msg;
            }

            return "";
        }
        catch
        {
            return "";
        }       
    }

    public bool BuyProcess(long price_ , float amount_)
    {
        if (ClickBuyTab() == false)
            return false;

        if (InputBuyPrice(price_) == false)
            return false;

        if (InputBuyAmount(amount_) == false)
            return false;

        string _msg = ClearAlertPopup();
        if (_msg.Contains("금액이 부족합니다"))
        {
            if (InputBuyPrice(price_) == false)
                return false;
            ClickMaxAmount();
        }

        if (Global.checkBox_StopAtFee)
        {
            double fee = GetTadeFee();
            if (fee > 0.0f)
            {
                PrintLog("수수료 발생 : " + fee);
                uiController.StopEvent();
            }
        }
            
        if (ClickBuy() == false)
        {
            ClearAlertPopup();
            return false;
        }

        if (CheckConfirmBox() == false)
            return false;

        if (ClickApproveOk() == false)
            return false;

        return true;
    }

    public bool SellProcess(long price_, float amount_)
    {
        if (ClickSellTab() == false)
            return false;

        if (InputSellPrice(price_) == false)
            return false;

        if (InputSellAmount(amount_) == false)
            return false;

        string _msg = ClearAlertPopup();
        if (_msg.Contains("수량이 부족합니다"))
        {
            if (InputSellPrice(price_) == false)
                return false;
            ClickMaxAmount();
        }

        if (Global.checkBox_StopAtFee)
        {
            double fee = GetTadeFee();
            if (fee > 0.0f)
            {
                PrintLog("수수료 발생 : " + fee);
                uiController.StopEvent();
            }
        }

        if (ClickSell() == false)
        {
            ClearAlertPopup();
            return false;
        }

        if (CheckConfirmBox() == false)
            return false;

        if (ClickApproveOk() == false)
            return false;

        return true;
    }

    public bool ClearOrder()
    {
        try
        {
            driver.Navigate().GoToUrl("https://www.bithumb.com/u2/cancelorder");
            Thread.Sleep(100);
            SelectElement elemCoin = new SelectElement(driver.FindElementByXPath("//*[@id='searchCoin']"));
            elemCoin.SelectByText(Global.comboBox_TradeCoin);
            Thread.Sleep(100);
            while (true)
            {
                IList<IWebElement> listOrder = FindMyElements(By.XPath("//*[@id='waitTradeLists']/tr"));
                if (listOrder.Count <= 1)
                {
                    if (listOrder.Count == 1)
                    {
                        string strHtml = listOrder[0].GetAttribute("innerHTML");
                        if (strHtml.Contains("no_list"))
                        {
                            break;
                        }
                        else
                        {
                            //1개의 항목이 남아 있는 케이스
                        }                       
                    }
                    else
                    {
                        break;
                    }
                        
                }
                    

                IWebElement checkAll = driver.FindElement(By.XPath("//*[@id='chkAll']"));
                checkAll.Click();
                Thread.Sleep(100);

                IWebElement btnCancel = driver.FindElement(By.ClassName("order_cancle_box")).FindElement(By.XPath("./a"));
                btnCancel.Click();
                Thread.Sleep(100);

                CheckConfirmBox();
                ClearAlertPopup();

            }

            PrintLog("모든 미체결 주문이 취소되었습니다.");
            return true;
        }
        catch
        {
            PrintLog("주문취소 실패");
            return false;
        }
    }
}

