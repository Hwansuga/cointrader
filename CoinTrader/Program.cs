﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoinTrader
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            string mutexKey = "{54DDDF29-1CEE-45B6-8336-26B9E86DCAB9}";

            //check duplication
            if (Util.CheckDuplication(mutexKey))
                return;

//             DateTime now = Util.GetCurDateFromInt();
// 
//             DateTime limitDate = new DateTime(2018, 12, 11);
//             int ret = DateTime.Compare(now, limitDate);
//             if (ret > 0)
//             {
//                 MessageBox.Show("기간이 만료되었습니다.");
//                 return;
//             }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
