﻿namespace CoinTrader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label_CoinTitle = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label_CurPrice = new System.Windows.Forms.Label();
            this.listBox_Log = new System.Windows.Forms.ListBox();
            this.button_CreateWnd = new System.Windows.Forms.Button();
            this.button_GoTradeCenter = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label_MyAmount = new System.Windows.Forms.Label();
            this.label_MyMoney = new System.Windows.Forms.Label();
            this.label_UseValue = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox_SellAmount = new System.Windows.Forms.TextBox();
            this.textBox_BuyAmount = new System.Windows.Forms.TextBox();
            this.textBox_SellPrice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_BuyPrice = new System.Windows.Forms.TextBox();
            this.groupBox_SettingInfo = new System.Windows.Forms.GroupBox();
            this.checkBox_StopAtTax = new System.Windows.Forms.CheckBox();
            this.checkBox_ClearOrderInCycle = new System.Windows.Forms.CheckBox();
            this.checkBox_ClearOrder = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox_MaxCapacity = new System.Windows.Forms.CheckBox();
            this.checkBox_MaxUseValueCapacity = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.numericUpDown_Timer = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox_TradeType = new System.Windows.Forms.ComboBox();
            this.numericUpDown_Times = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_MaxCapacity = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_LimitDelay = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_MaxUseValueCapacity = new System.Windows.Forms.NumericUpDown();
            this.checkBox_Timer = new System.Windows.Forms.CheckBox();
            this.comboBox_TradeCoin = new System.Windows.Forms.ComboBox();
            this.button_Start = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.tableLayoutPanel_Timer = new System.Windows.Forms.TableLayoutPanel();
            this.label_RemainSec = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox_SettingInfo.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Timer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Times)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxCapacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxUseValueCapacity)).BeginInit();
            this.tableLayoutPanel_Timer.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_CoinTitle
            // 
            this.label_CoinTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_CoinTitle.AutoSize = true;
            this.label_CoinTitle.Location = new System.Drawing.Point(88, 3);
            this.label_CoinTitle.Margin = new System.Windows.Forms.Padding(0);
            this.label_CoinTitle.Name = "label_CoinTitle";
            this.label_CoinTitle.Size = new System.Drawing.Size(83, 18);
            this.label_CoinTitle.TabIndex = 2;
            this.label_CoinTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(268, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Test";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label_CurPrice
            // 
            this.label_CurPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_CurPrice.AutoSize = true;
            this.label_CurPrice.Location = new System.Drawing.Point(88, 24);
            this.label_CurPrice.Margin = new System.Windows.Forms.Padding(0);
            this.label_CurPrice.Name = "label_CurPrice";
            this.label_CurPrice.Size = new System.Drawing.Size(83, 18);
            this.label_CurPrice.TabIndex = 4;
            this.label_CurPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBox_Log
            // 
            this.listBox_Log.FormattingEnabled = true;
            this.listBox_Log.ItemHeight = 12;
            this.listBox_Log.Location = new System.Drawing.Point(309, 37);
            this.listBox_Log.Name = "listBox_Log";
            this.listBox_Log.Size = new System.Drawing.Size(367, 412);
            this.listBox_Log.TabIndex = 5;
            // 
            // button_CreateWnd
            // 
            this.button_CreateWnd.Location = new System.Drawing.Point(12, 337);
            this.button_CreateWnd.Name = "button_CreateWnd";
            this.button_CreateWnd.Size = new System.Drawing.Size(92, 23);
            this.button_CreateWnd.TabIndex = 6;
            this.button_CreateWnd.Text = "창 생성";
            this.button_CreateWnd.UseVisualStyleBackColor = true;
            this.button_CreateWnd.Click += new System.EventHandler(this.button_CreateWnd_Click);
            // 
            // button_GoTradeCenter
            // 
            this.button_GoTradeCenter.Location = new System.Drawing.Point(12, 366);
            this.button_GoTradeCenter.Name = "button_GoTradeCenter";
            this.button_GoTradeCenter.Size = new System.Drawing.Size(92, 23);
            this.button_GoTradeCenter.TabIndex = 7;
            this.button_GoTradeCenter.Text = "거래소 입장";
            this.button_GoTradeCenter.UseVisualStyleBackColor = true;
            this.button_GoTradeCenter.Click += new System.EventHandler(this.button_GoTradeCenter_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label_CurPrice, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label_CoinTitle, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_MyAmount, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label_MyMoney, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label_UseValue, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(129, 339);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(174, 110);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 66);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 18);
            this.label12.TabIndex = 9;
            this.label12.Text = "보유금액";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "현재가";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "화폐 명";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 45);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 18);
            this.label11.TabIndex = 7;
            this.label11.Text = "보유수량";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_MyAmount
            // 
            this.label_MyAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_MyAmount.AutoSize = true;
            this.label_MyAmount.Location = new System.Drawing.Point(88, 45);
            this.label_MyAmount.Margin = new System.Windows.Forms.Padding(0);
            this.label_MyAmount.Name = "label_MyAmount";
            this.label_MyAmount.Size = new System.Drawing.Size(83, 18);
            this.label_MyAmount.TabIndex = 8;
            this.label_MyAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_MyMoney
            // 
            this.label_MyMoney.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_MyMoney.AutoSize = true;
            this.label_MyMoney.Location = new System.Drawing.Point(88, 66);
            this.label_MyMoney.Margin = new System.Windows.Forms.Padding(0);
            this.label_MyMoney.Name = "label_MyMoney";
            this.label_MyMoney.Size = new System.Drawing.Size(83, 18);
            this.label_MyMoney.TabIndex = 10;
            this.label_MyMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_UseValue
            // 
            this.label_UseValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_UseValue.AutoSize = true;
            this.label_UseValue.Location = new System.Drawing.Point(88, 87);
            this.label_UseValue.Margin = new System.Windows.Forms.Padding(0);
            this.label_UseValue.Name = "label_UseValue";
            this.label_UseValue.Size = new System.Drawing.Size(83, 20);
            this.label_UseValue.TabIndex = 12;
            this.label_UseValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 87);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 20);
            this.label15.TabIndex = 11;
            this.label15.Text = "거래금액";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.textBox_SellAmount, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBox_BuyAmount, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBox_SellPrice, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label5, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label6, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.textBox_BuyPrice, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 20);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(275, 65);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // textBox_SellAmount
            // 
            this.textBox_SellAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_SellAmount.Location = new System.Drawing.Point(184, 44);
            this.textBox_SellAmount.Margin = new System.Windows.Forms.Padding(1);
            this.textBox_SellAmount.Name = "textBox_SellAmount";
            this.textBox_SellAmount.Size = new System.Drawing.Size(89, 21);
            this.textBox_SellAmount.TabIndex = 14;
            // 
            // textBox_BuyAmount
            // 
            this.textBox_BuyAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_BuyAmount.Location = new System.Drawing.Point(93, 44);
            this.textBox_BuyAmount.Margin = new System.Windows.Forms.Padding(1);
            this.textBox_BuyAmount.Name = "textBox_BuyAmount";
            this.textBox_BuyAmount.Size = new System.Drawing.Size(88, 21);
            this.textBox_BuyAmount.TabIndex = 13;
            // 
            // textBox_SellPrice
            // 
            this.textBox_SellPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_SellPrice.Location = new System.Drawing.Point(184, 23);
            this.textBox_SellPrice.Margin = new System.Windows.Forms.Padding(1);
            this.textBox_SellPrice.Name = "textBox_SellPrice";
            this.textBox_SellPrice.Size = new System.Drawing.Size(89, 21);
            this.textBox_SellPrice.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 21);
            this.label7.TabIndex = 10;
            this.label7.Text = "수량";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "=====";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "가격";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(95, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "매수";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(186, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "매도";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_BuyPrice
            // 
            this.textBox_BuyPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_BuyPrice.Location = new System.Drawing.Point(93, 23);
            this.textBox_BuyPrice.Margin = new System.Windows.Forms.Padding(1);
            this.textBox_BuyPrice.Name = "textBox_BuyPrice";
            this.textBox_BuyPrice.Size = new System.Drawing.Size(88, 21);
            this.textBox_BuyPrice.TabIndex = 11;
            // 
            // groupBox_SettingInfo
            // 
            this.groupBox_SettingInfo.Controls.Add(this.checkBox_StopAtTax);
            this.groupBox_SettingInfo.Controls.Add(this.checkBox_ClearOrderInCycle);
            this.groupBox_SettingInfo.Controls.Add(this.checkBox_ClearOrder);
            this.groupBox_SettingInfo.Controls.Add(this.tableLayoutPanel3);
            this.groupBox_SettingInfo.Controls.Add(this.tableLayoutPanel2);
            this.groupBox_SettingInfo.Location = new System.Drawing.Point(13, 13);
            this.groupBox_SettingInfo.Name = "groupBox_SettingInfo";
            this.groupBox_SettingInfo.Size = new System.Drawing.Size(290, 320);
            this.groupBox_SettingInfo.TabIndex = 12;
            this.groupBox_SettingInfo.TabStop = false;
            this.groupBox_SettingInfo.Text = "Setting Infomation";
            // 
            // checkBox_StopAtTax
            // 
            this.checkBox_StopAtTax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox_StopAtTax.AutoSize = true;
            this.checkBox_StopAtTax.Location = new System.Drawing.Point(6, 254);
            this.checkBox_StopAtTax.Name = "checkBox_StopAtTax";
            this.checkBox_StopAtTax.Size = new System.Drawing.Size(128, 16);
            this.checkBox_StopAtTax.TabIndex = 15;
            this.checkBox_StopAtTax.Text = "수수료 발생시 정지";
            this.checkBox_StopAtTax.UseVisualStyleBackColor = true;
            // 
            // checkBox_ClearOrderInCycle
            // 
            this.checkBox_ClearOrderInCycle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox_ClearOrderInCycle.AutoSize = true;
            this.checkBox_ClearOrderInCycle.Checked = true;
            this.checkBox_ClearOrderInCycle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_ClearOrderInCycle.Location = new System.Drawing.Point(6, 276);
            this.checkBox_ClearOrderInCycle.Name = "checkBox_ClearOrderInCycle";
            this.checkBox_ClearOrderInCycle.Size = new System.Drawing.Size(140, 16);
            this.checkBox_ClearOrderInCycle.TabIndex = 14;
            this.checkBox_ClearOrderInCycle.Text = "미체결 주문 자동취소";
            this.checkBox_ClearOrderInCycle.UseVisualStyleBackColor = true;
            // 
            // checkBox_ClearOrder
            // 
            this.checkBox_ClearOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox_ClearOrder.AutoSize = true;
            this.checkBox_ClearOrder.Checked = true;
            this.checkBox_ClearOrder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_ClearOrder.Location = new System.Drawing.Point(6, 298);
            this.checkBox_ClearOrder.Name = "checkBox_ClearOrder";
            this.checkBox_ClearOrder.Size = new System.Drawing.Size(144, 16);
            this.checkBox_ClearOrder.TabIndex = 13;
            this.checkBox_ClearOrder.Text = "정지시 모든 주문 취소";
            this.checkBox_ClearOrder.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.checkBox_MaxCapacity, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.checkBox_MaxUseValueCapacity, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_Timer, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.comboBox_TradeType, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_Times, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_MaxCapacity, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_LimitDelay, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_MaxUseValueCapacity, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.checkBox_Timer, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.comboBox_TradeCoin, 1, 6);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 91);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(275, 155);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // checkBox_MaxCapacity
            // 
            this.checkBox_MaxCapacity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_MaxCapacity.AutoSize = true;
            this.checkBox_MaxCapacity.Location = new System.Drawing.Point(3, 47);
            this.checkBox_MaxCapacity.Name = "checkBox_MaxCapacity";
            this.checkBox_MaxCapacity.Size = new System.Drawing.Size(131, 16);
            this.checkBox_MaxCapacity.TabIndex = 24;
            this.checkBox_MaxCapacity.Text = "최대보유 수량";
            this.checkBox_MaxCapacity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox_MaxCapacity.UseVisualStyleBackColor = true;
            this.checkBox_MaxCapacity.CheckedChanged += new System.EventHandler(this.checkBox_MaxCapacity_CheckedChanged);
            // 
            // checkBox_MaxUseValueCapacity
            // 
            this.checkBox_MaxUseValueCapacity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_MaxUseValueCapacity.AutoSize = true;
            this.checkBox_MaxUseValueCapacity.Location = new System.Drawing.Point(3, 69);
            this.checkBox_MaxUseValueCapacity.Name = "checkBox_MaxUseValueCapacity";
            this.checkBox_MaxUseValueCapacity.Size = new System.Drawing.Size(131, 16);
            this.checkBox_MaxUseValueCapacity.TabIndex = 23;
            this.checkBox_MaxUseValueCapacity.Text = "거래금액 제한";
            this.checkBox_MaxUseValueCapacity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox_MaxUseValueCapacity.UseVisualStyleBackColor = true;
            this.checkBox_MaxUseValueCapacity.CheckedChanged += new System.EventHandler(this.checkBox_MaxUseValueCapacity_CheckedChanged);
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 132);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(131, 23);
            this.label16.TabIndex = 24;
            this.label16.Text = "거래 코인";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_Timer
            // 
            this.numericUpDown_Timer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_Timer.Enabled = false;
            this.numericUpDown_Timer.Location = new System.Drawing.Point(138, 111);
            this.numericUpDown_Timer.Margin = new System.Windows.Forms.Padding(1);
            this.numericUpDown_Timer.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_Timer.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Timer.Name = "numericUpDown_Timer";
            this.numericUpDown_Timer.Size = new System.Drawing.Size(136, 21);
            this.numericUpDown_Timer.TabIndex = 23;
            this.numericUpDown_Timer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_Timer.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(131, 22);
            this.label14.TabIndex = 19;
            this.label14.Text = "딜레이(초)";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 22);
            this.label9.TabIndex = 13;
            this.label9.Text = "반복횟수";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(131, 22);
            this.label8.TabIndex = 11;
            this.label8.Text = "매매타입";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBox_TradeType
            // 
            this.comboBox_TradeType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_TradeType.FormattingEnabled = true;
            this.comboBox_TradeType.Items.AddRange(new object[] {
            "매수->매도",
            "매도->매수",
            "매수",
            "매도"});
            this.comboBox_TradeType.Location = new System.Drawing.Point(138, 1);
            this.comboBox_TradeType.Margin = new System.Windows.Forms.Padding(1);
            this.comboBox_TradeType.Name = "comboBox_TradeType";
            this.comboBox_TradeType.Size = new System.Drawing.Size(136, 20);
            this.comboBox_TradeType.TabIndex = 12;
            // 
            // numericUpDown_Times
            // 
            this.numericUpDown_Times.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_Times.Location = new System.Drawing.Point(138, 23);
            this.numericUpDown_Times.Margin = new System.Windows.Forms.Padding(1);
            this.numericUpDown_Times.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_Times.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Times.Name = "numericUpDown_Times";
            this.numericUpDown_Times.Size = new System.Drawing.Size(136, 21);
            this.numericUpDown_Times.TabIndex = 14;
            this.numericUpDown_Times.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_Times.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown_MaxCapacity
            // 
            this.numericUpDown_MaxCapacity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_MaxCapacity.Enabled = false;
            this.numericUpDown_MaxCapacity.Location = new System.Drawing.Point(138, 45);
            this.numericUpDown_MaxCapacity.Margin = new System.Windows.Forms.Padding(1);
            this.numericUpDown_MaxCapacity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_MaxCapacity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_MaxCapacity.Name = "numericUpDown_MaxCapacity";
            this.numericUpDown_MaxCapacity.Size = new System.Drawing.Size(136, 21);
            this.numericUpDown_MaxCapacity.TabIndex = 16;
            this.numericUpDown_MaxCapacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_MaxCapacity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown_LimitDelay
            // 
            this.numericUpDown_LimitDelay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_LimitDelay.Location = new System.Drawing.Point(138, 89);
            this.numericUpDown_LimitDelay.Margin = new System.Windows.Forms.Padding(1);
            this.numericUpDown_LimitDelay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_LimitDelay.Name = "numericUpDown_LimitDelay";
            this.numericUpDown_LimitDelay.Size = new System.Drawing.Size(136, 21);
            this.numericUpDown_LimitDelay.TabIndex = 20;
            this.numericUpDown_LimitDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numericUpDown_MaxUseValueCapacity
            // 
            this.numericUpDown_MaxUseValueCapacity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown_MaxUseValueCapacity.Enabled = false;
            this.numericUpDown_MaxUseValueCapacity.Location = new System.Drawing.Point(138, 67);
            this.numericUpDown_MaxUseValueCapacity.Margin = new System.Windows.Forms.Padding(1);
            this.numericUpDown_MaxUseValueCapacity.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDown_MaxUseValueCapacity.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown_MaxUseValueCapacity.Name = "numericUpDown_MaxUseValueCapacity";
            this.numericUpDown_MaxUseValueCapacity.Size = new System.Drawing.Size(136, 21);
            this.numericUpDown_MaxUseValueCapacity.TabIndex = 21;
            this.numericUpDown_MaxUseValueCapacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_MaxUseValueCapacity.Value = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            // 
            // checkBox_Timer
            // 
            this.checkBox_Timer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Timer.AutoSize = true;
            this.checkBox_Timer.Location = new System.Drawing.Point(3, 113);
            this.checkBox_Timer.Name = "checkBox_Timer";
            this.checkBox_Timer.Size = new System.Drawing.Size(131, 16);
            this.checkBox_Timer.TabIndex = 22;
            this.checkBox_Timer.Text = "타이머(초)";
            this.checkBox_Timer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox_Timer.UseVisualStyleBackColor = true;
            this.checkBox_Timer.CheckedChanged += new System.EventHandler(this.checkBox_Timer_CheckedChanged);
            // 
            // comboBox_TradeCoin
            // 
            this.comboBox_TradeCoin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_TradeCoin.FormattingEnabled = true;
            this.comboBox_TradeCoin.Items.AddRange(new object[] {
            "매수->매도",
            "매도->매수",
            "매수",
            "매도"});
            this.comboBox_TradeCoin.Location = new System.Drawing.Point(138, 133);
            this.comboBox_TradeCoin.Margin = new System.Windows.Forms.Padding(1);
            this.comboBox_TradeCoin.Name = "comboBox_TradeCoin";
            this.comboBox_TradeCoin.Size = new System.Drawing.Size(136, 20);
            this.comboBox_TradeCoin.TabIndex = 25;
            this.comboBox_TradeCoin.SelectedIndexChanged += new System.EventHandler(this.comboBox_TradeCoin_SelectedIndexChanged);
            // 
            // button_Start
            // 
            this.button_Start.Location = new System.Drawing.Point(12, 395);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(92, 23);
            this.button_Start.TabIndex = 13;
            this.button_Start.Text = "시작";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.Location = new System.Drawing.Point(12, 424);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(92, 23);
            this.button_Stop.TabIndex = 14;
            this.button_Stop.Text = "정지";
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // tableLayoutPanel_Timer
            // 
            this.tableLayoutPanel_Timer.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tableLayoutPanel_Timer.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel_Timer.ColumnCount = 2;
            this.tableLayoutPanel_Timer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_Timer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_Timer.Controls.Add(this.label_RemainSec, 0, 0);
            this.tableLayoutPanel_Timer.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel_Timer.Enabled = false;
            this.tableLayoutPanel_Timer.Location = new System.Drawing.Point(500, 12);
            this.tableLayoutPanel_Timer.Name = "tableLayoutPanel_Timer";
            this.tableLayoutPanel_Timer.RowCount = 1;
            this.tableLayoutPanel_Timer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_Timer.Size = new System.Drawing.Size(176, 19);
            this.tableLayoutPanel_Timer.TabIndex = 15;
            // 
            // label_RemainSec
            // 
            this.label_RemainSec.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_RemainSec.AutoSize = true;
            this.label_RemainSec.Location = new System.Drawing.Point(88, 1);
            this.label_RemainSec.Margin = new System.Windows.Forms.Padding(0);
            this.label_RemainSec.Name = "label_RemainSec";
            this.label_RemainSec.Size = new System.Drawing.Size(87, 17);
            this.label_RemainSec.TabIndex = 7;
            this.label_RemainSec.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 1);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 17);
            this.label17.TabIndex = 6;
            this.label17.Text = "남은시간(초)";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 455);
            this.Controls.Add(this.tableLayoutPanel_Timer);
            this.Controls.Add(this.button_Stop);
            this.Controls.Add(this.button_Start);
            this.Controls.Add(this.groupBox_SettingInfo);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.button_GoTradeCenter);
            this.Controls.Add(this.button_CreateWnd);
            this.Controls.Add(this.listBox_Log);
            this.Controls.Add(this.button2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "CoinTrader";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox_SettingInfo.ResumeLayout(false);
            this.groupBox_SettingInfo.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Timer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Times)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxCapacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxUseValueCapacity)).EndInit();
            this.tableLayoutPanel_Timer.ResumeLayout(false);
            this.tableLayoutPanel_Timer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label_CoinTitle;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label_CurPrice;
        private System.Windows.Forms.ListBox listBox_Log;
        private System.Windows.Forms.Button button_CreateWnd;
        private System.Windows.Forms.Button button_GoTradeCenter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_SellAmount;
        private System.Windows.Forms.TextBox textBox_BuyAmount;
        private System.Windows.Forms.TextBox textBox_SellPrice;
        private System.Windows.Forms.TextBox textBox_BuyPrice;
        private System.Windows.Forms.GroupBox groupBox_SettingInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox_TradeType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDown_Times;
        private System.Windows.Forms.NumericUpDown numericUpDown_MaxCapacity;
        private System.Windows.Forms.CheckBox checkBox_ClearOrder;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label_MyAmount;
        private System.Windows.Forms.Label label_MyMoney;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label_UseValue;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numericUpDown_LimitDelay;
        private System.Windows.Forms.NumericUpDown numericUpDown_MaxUseValueCapacity;
        private System.Windows.Forms.CheckBox checkBox_Timer;
        private System.Windows.Forms.NumericUpDown numericUpDown_Timer;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBox_TradeCoin;
        private System.Windows.Forms.CheckBox checkBox_ClearOrderInCycle;
        private System.Windows.Forms.CheckBox checkBox_StopAtTax;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Timer;
        private System.Windows.Forms.Label label_RemainSec;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox checkBox_MaxUseValueCapacity;
        private System.Windows.Forms.CheckBox checkBox_MaxCapacity;
    }
}

