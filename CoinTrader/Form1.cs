﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoinTrader
{
    public partial class Form1 : Form
    {
        Bithumb bithum = null;

        System.Windows.Forms.Timer timer1Sec = new System.Windows.Forms.Timer();

        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bithum.GoTradePage();

            bithum.ClickMaxAmount();
        }

        private void button_CreateWnd_Click(object sender, EventArgs e)
        {
            bithum = new Bithumb(this);
            if (bithum.Login())
            {
                comboBox_TradeCoin.Items.Clear();
                foreach (var item in Global.listCoin)
                {
                    comboBox_TradeCoin.Items.Add(item);
                }
                    

                PrintLog("창 생성 완료");
                PrintLog("로그인 해주세요");

                button_CreateWnd.Enabled = false;
            }           
        }

        private void button_GoTradeCenter_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Global.comboBox_TradeCoin))
            {
                PrintLog("거래 코인을 선택해 주세요");
                return;
            }

            bithum.GoTradePage();
            PrintLog("거래소 입장");

            UpdateCurrentMyInfo();
            button_GoTradeCenter.Enabled = false;
        }

        public void PrintLog(string msg_)
        {
            listBox_Log.Invoke(new MethodInvoker(delegate ()
            {
                Util.AutoLineStringAdd(listBox_Log , msg_);
            }));
        }

        public void UpdateCurrentMyInfo()
        {
            if (bithum == null)
                return;

            this.Invoke(new MethodInvoker(delegate () {
                label_CoinTitle.Text = bithum.GetCointTitle();
                label_CurPrice.Text = bithum.GetCurrentPrice();
                label_MyAmount.Text = bithum.GetMyAmount();
                label_MyMoney.Text = bithum.GetMyMoney();
                label_UseValue.Text = bithum.GetTradingValue();
            }));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Util.KillProcess("chromedriver");
            ConnectCustomEvent();

            timer1Sec.Interval = 1000;
            timer1Sec.Tick += new EventHandler(SectionInfoRemainTime_Tick);
            timer1Sec.Start();

#if TEST
            button2.Visible = true;
#else
            button2.Visible = false;
#endif


        }

        void SectionInfoRemainTime_Tick(object sender, EventArgs e)
        {
            if (Global.stop == true)
                return;

            if (Global.checkBox_Timer == false)
                return;

            if (Global.numericUpDown_Timer <= 0)
                return;

            Global.numericUpDown_Timer--;

            label_RemainSec.Text = Global.numericUpDown_Timer.ToString();

            if (Global.numericUpDown_Timer == 0)
            {
                PrintLog("타이머 종료");
                StopEvent();
            }
        }

        void ConnectCustomEvent()
        {
            List<FieldInfo> listField = GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();

            List<FieldInfo> listNumericUpDown = listField.FindAll(x => x.FieldType == typeof(NumericUpDown));
            foreach (FieldInfo item in listNumericUpDown)
            {
                NumericUpDown temp = (NumericUpDown)item.GetValue(this);
                temp.Click += EventUtil.numericUpDownCustom_ValueChanged;
                temp.Leave += EventUtil.numericUpDownCustom_ValueChanged;
            }

            List<FieldInfo> listTextBox = listField.FindAll(x => x.FieldType == typeof(TextBox));
            foreach (FieldInfo item in listTextBox)
            {
                TextBox temp = (TextBox)item.GetValue(this);
                temp.TextChanged += EventUtil.textBoxCustom_TextChanged;
            }

            List<FieldInfo> listLabel = listField.FindAll(x => x.FieldType == typeof(Label));
            foreach (FieldInfo item in listLabel)
            {
                Label temp = (Label)item.GetValue(this);
                temp.TextChanged += EventUtil.labelCustom_TextChanged;
            }

            List<FieldInfo> listCheckBox = listField.FindAll(x => x.FieldType == typeof(CheckBox));
            foreach (FieldInfo item in listCheckBox)
            {
                CheckBox temp = (CheckBox)item.GetValue(this);
                temp.CheckedChanged += EventUtil.checkBoxCustom_CheckedChanged;
            }

            List<FieldInfo> listComboBox = listField.FindAll(x => x.FieldType == typeof(ComboBox));
            foreach (FieldInfo item in listComboBox)
            {
                ComboBox temp = (ComboBox)item.GetValue(this);
                temp.SelectedIndexChanged += EventUtil.comboBoxCustom_CheckedChanged;
            }
        }

        public void WorkThread()
        {
            long buyPrice = long.Parse(Global.textBox_BuyPrice);
            long sellPrice = long.Parse(Global.textBox_SellPrice);
            float buyAmount = float.Parse(Global.textBox_BuyAmount);
            float sellAmount = float.Parse(Global.textBox_SellAmount);

            for (int i=0; i<Global.numericUpDown_Times; ++i)
            {
                if (Global.stop)
                    break;

                switch(Global.comboBox_TradeType)
                {
                    case "매수->매도":
                        bithum.BuyProcess(buyPrice , buyAmount);
                        bithum.SellProcess(sellPrice, sellAmount);
                        break;
                    case "매도->매수":
                        bithum.SellProcess(sellPrice, sellAmount);
                        bithum.BuyProcess(buyPrice, buyAmount);                       
                        break;
                    case "매수":
                        bithum.BuyProcess(buyPrice, buyAmount);
                        break;
                    case "매도":
                        bithum.SellProcess(sellPrice, sellAmount);
                        break;
                }

                if (Global.numericUpDown_LimitDelay > 0)
                    Thread.Sleep(Global.numericUpDown_LimitDelay);
            
                if (Global.checkBox_ClearOrderInCycle)
                {
                    bithum.ClearOrder();
                    bithum.GoTradePage();
                }
                    
                UpdateCurrentMyInfo();

                if (CheckStopCondition())
                    break;

            }

            if (Global.checkBox_ClearOrder)
            {
                bithum.ClearOrder();
            }

            StopEvent();
            DoneToStop();
            PrintLog("정지 완료");
        }

        bool CheckStopCondition()
        {
            
            if (Global.checkBox_MaxCapacity)
            {
                float myAmount = float.Parse(Global.label_MyAmount);
                if (myAmount >= Global.numericUpDown_MaxCapacity)
                {
                    PrintLog("보유수량이 한계치 " + Global.numericUpDown_MaxCapacity + "에 도달하였습니다.");
                    return true;
                }
            }
            
            if (Global.checkBox_MaxUseValueCapacity)
            {
                if (string.IsNullOrEmpty(Global.label_UseValue))
                    return false;

                float useValue = float.Parse(Global.label_UseValue);
                if (useValue >= Global.numericUpDown_MaxUseValueCapacity)
                {
                    PrintLog("거래금액이  " + Global.numericUpDown_MaxUseValueCapacity + "에 도달하였습니다.");
                    return true;
                }
            }
           
            return false;
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Global.comboBox_TradeType))
            {
                MessageBox.Show("매매타입을 선택해 주세요");
                return;
            }

            if (string.IsNullOrEmpty(Global.textBox_BuyPrice)
                || string.IsNullOrEmpty(Global.textBox_BuyAmount)
                || string.IsNullOrEmpty(Global.textBox_SellPrice)
                || string.IsNullOrEmpty(Global.textBox_SellAmount))
            {
                MessageBox.Show("매매정보를 입력해 주세요");
                return;
            }

            Global.stop = false;

            Thread hWorkThread = new Thread(new ThreadStart(WorkThread));
            hWorkThread.Start();

            button_Stop.Invoke(new MethodInvoker(delegate ()
            {
                button_Stop.Enabled = true;
                groupBox_SettingInfo.Enabled = false;
            }));
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            StopEvent();          
        }

        public void StopEvent()
        {
            Global.stop = true;
            PrintLog("정지 중...");
            button_Stop.Invoke(new MethodInvoker(delegate() {
                button_Stop.Enabled = false;
            }));
        }

        void DoneToStop()
        {
            this.Invoke(new MethodInvoker(delegate() {
                button_Start.Enabled = true;
                button_Stop.Enabled = false;

                groupBox_SettingInfo.Enabled = true;

            }));

            bithum.GoTradePage();
        }

        private void checkBox_Timer_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_Timer.Enabled = checkBox_Timer.Checked;
            tableLayoutPanel_Timer.Enabled = checkBox_Timer.Checked;
        }

        private void checkBox_MaxUseValueCapacity_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_MaxUseValueCapacity.Enabled = checkBox_MaxUseValueCapacity.Checked;
        }

        private void checkBox_MaxCapacity_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_MaxCapacity.Enabled = checkBox_MaxCapacity.Checked;
        }

        private void comboBox_TradeCoin_SelectedIndexChanged(object sender, EventArgs e)
        {
            button_GoTradeCenter.Enabled = true;
        }
    }
}
