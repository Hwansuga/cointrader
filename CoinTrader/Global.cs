﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Global:Singleton<Global>
{
    public static bool stop = true;
    public static List<string> listCoin = new List<string>();

    public static string label_CoinTitle = "";
    public static string label_CurPrice = "";
    public static string label_MyAmount = "";
    public static string label_MyMoney = "";
    public static string label_UseValue = "";

    public static string textBox_BuyPrice = "";
    public static string textBox_BuyAmount = "";
    public static string textBox_SellPrice = "";
    public static string textBox_SellAmount = "";

    public static string comboBox_TradeType = "";
    public static int numericUpDown_Times = 1;
    public static bool checkBox_MaxCapacity = false;
    public static int numericUpDown_MaxCapacity = 1;
    public static bool checkBox_MaxUseValueCapacity = false;
    public static int numericUpDown_MaxUseValueCapacity = 100000000;
    public static int numericUpDown_LimitDelay = 0;
    public static bool checkBox_Timer = false;
    public static int numericUpDown_Timer = 1;
    public static string comboBox_TradeCoin = "";

    public static bool checkBox_StopAtFee = false;
    public static bool checkBox_ClearOrder = true;
    public static bool checkBox_ClearOrderInCycle = true;
}

